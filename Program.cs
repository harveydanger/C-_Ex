﻿/*
Multithreading application for solving producer consumer problem.
 */
using System;
using System.Collections.Generic;
using System.Threading;
namespace producer_consumer
{
    class Program
    {
        /**
        This is a good idea to have a GLOBAL constants (don't confuse with variables) because all the adjustment goes here
        Try changing values and see the behavior
         */
#region Constants
        const int NUM_OPS = 20;
        const int DEF_SLEEP_TIME_P = 200;
        const int DEF_SLEEP_TIME_C = 300;


#endregion
        static void Main(string[] args)
        {
            Thread.CurrentThread.Name = "Controller thread";

            Queue qobj = new Queue(len: NUM_OPS);
            List<int> q = qobj.getQueue(); // dereference for a quicker access
            Producer p = new Producer();
            Consumer c = new Consumer();
            Lock l = new Lock();
            var pt = new Thread(()=>{ // you could use a delegate here, but for the sake of simplicity it is a closure.
                Thread.CurrentThread.Name = "Producer thread";
                for(int i = 0; i< NUM_OPS; i++){
                    p.Produce(qobj, l);
                    Thread.Sleep(DEF_SLEEP_TIME_P);
                }
                p.stillProducing = false;
            });

            var ct = new Thread(()=>{
                Thread.CurrentThread.Name = "Consumer thread";
                while(p.stillProducing){
                    Thread.Sleep(DEF_SLEEP_TIME_C);
                    while(q.Count > 0){
                        c.Consume(qobj, l);
                        Thread.Sleep(DEF_SLEEP_TIME_C);
                    }
                }
            });

            pt.Start();
            ct.Start();
            pt.Join();
            ct.Join();
            qobj.ReportStatus();
        }
    }
}
