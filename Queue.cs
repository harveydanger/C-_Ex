using System;
using System.Collections.Generic;

namespace producer_consumer{
    /*
        Here Queue is used not as a Generic class Queue (e.g. System.Collections.Generic.Queue), but as a list to show you
        how it basically works
     */
    public class Queue{
        private List<int> queue;

        public Queue(int len){
            this.queue = new List<int>(len);
        }

        public void ReportStatus(){
            Console.WriteLine("There are currently {0} elements in the queue", this.queue.Count);
        }

        public void Enqueue(int item){
            this.queue.Add(item);
        }

        public int Dequeue(){
            int val = this.queue[0];
            this.shiftQueue();
            return val;
        }

        public List<int> getQueue()
        {
            return this.queue;
        }
        

        private void shiftQueue(){ // this is what actually happens in the Queue class
            for(int i =0; i< this.queue.Count; i++){
                if(i+1 >= this.queue.Count){
                    break;
                }
                this.queue[i] = this.queue[i+1];
            }
            this.queue.RemoveAt(this.queue.Count -1);
            
        }
        
    }
}