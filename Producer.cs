using System;
using System.Collections.Generic;

namespace producer_consumer{
    class Producer{

        public bool stillProducing; // important flag to show consumer that WIP
        public void Produce(Queue q, Lock _lock){
            lock(_lock){ // lock ensures that threads do not access the same data at the same time
                Console.WriteLine("Produced");
                q.Enqueue(1);
            }
        }
        public Producer(){
            this.stillProducing = true; // initialised in a constructor, though you can do it in a property directly
        }
    }
}