using System;
using System.Collections.Generic;

namespace producer_consumer{
    class Consumer{
        public void Consume(Queue q, Lock _lock){
            lock(_lock){ // same as in a producer
                Console.WriteLine("Consumed");
                if(q.getQueue().Count > 0){
                    q.Dequeue();
                }
            }
        }
    }
}